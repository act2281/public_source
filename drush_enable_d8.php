<?php

$enable_modules = array(
	"views", 
	"views_ui",
	"ctools",
	"webform",
	"admin_menu",
	"adminimal_admin_menu",
	"imce",
	"captcha",
	"page_title",
	"ckeditor",
	"drd_server",
	"aes",
	"module_filter",
	"metatag",
	"redirect",
	"xmlsitemap",
	"panels",
);

$disable_modules = array(
	"toolbar",
	"comment",
	"rdf",
	"overlay",
);

foreach($disable_modules as $module) {
	shell_exec("drush pm-disable ".$module." -y");
}
foreach($enable_modules as $module) {
	shell_exec("drush pm-enable ".$module." -y");
}

//Copy SSH Files
/*
	if (!file_exists('.ssh')) {
		mkdir("../.ssh",0700,true);
		chown('../.ssh',$opts['user']);
		chgrp('../.ssh',$opts['user']);
	}
	if(!copy ('/website_includes/sshkeypair/id_rsa','../.ssh/id_rsa')){
		echo 'copy failed';
	}
	chown('../.ssh/id_rsa',$opts['user']);
	chgrp('../.ssh/id_rsa',$opts['user']);
	chmod("../.ssh/id_rsa",0600);
	
	if(!copy ('/website_includes/sshkeypair/id_rsa.pub','../.ssh/id_rsa.pub')){
		echo 'copy failed';
	}
	chown('../.ssh/id_rsa.pub',$opts['user']);
	chgrp('../.ssh/id_rsa.pub',$opts['user']);
	chmod("../.ssh/id_rsa.pub",0600);
*/
