# Composer template for Drupal projects
/**
 * @mainpage 
 * Drupal 8 Installer Documentation
 * @defgroup readme Instructions
 * @{
 * Composer template for Drupal projects
 *
 * Additional documentation paragraphs go here.
 */

(functions, classes, etc. that belong as members of the group go here)

/**
 * @} End of "defgroup readme".
 */



[![Build Status](https://travis-ci.org/drupal-composer/drupal-project.svg?branch=8.x)](https://travis-ci.org/drupal-composer/drupal-project)

This project template provides a starter kit for managing your site
dependencies with [Composer](https://getcomposer.org/).

If you want to know how to use it as replacement for
[Drush Make](https://github.com/drush-ops/drush/blob/8.x/docs/make.md) visit
the [Documentation on drupal.org](https://www.drupal.org/node/2471553).

## Usage

First you need to [install composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).

> Note: The instructions below refer to the [global composer installation](https://getcomposer.org/doc/00-intro.md#globally).
You might need to replace `composer` with `php composer.phar` (or similar) 
for your setup.

After that you can create the project:

```
composer create-project vendor-act360/act360-drupal8 some-dir --stability dev --no-interaction
```

With `composer require ...` you can download new dependencies to your 
installation.

```
cd some-dir
composer require drupal/devel:~1.0
```

Use bash script to create the installation and environment
```
cd ~/USER
/bin/bash /website_scripts/drupal8
```
